<?php

/**
 * MainController
 * 
 * Base controller for other application controllers
 * 
 * @author Paweł Kaźmierczak <pkazmierczak007@gmail.com>
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class MainController extends AbstractActionController {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var ViewModel 
     */
    protected $view;

    /**
     * Init action used in child controllers
     * Create ViewModel instance and set doctrine entity manager
     */
    public function _init() {
        $this->setEntityManager();
        $this->view = new ViewModel();
    }

    /**
     * Set doctrine entity manager
     */
    public function setEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        }
    }

}
