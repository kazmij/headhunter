<?php

/**
 * IndexController
 * 
 * @author Paweł Kaźmierczak <pkazmierczak007@gmail.com>
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\MainController;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Application\Entity\Location;
use Application\Repository\LocationRepository;
use Application\Entity\Comment;
use Application\Form\LocationType;
use Application\Form\CommentType;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class IndexController extends MainController {
    
    const DISTANCE = 2; //2 km from address 

    /**
     * Index action - list all locations, filter and order
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        // get order and order dir params
        $order_by = $this->params()->fromRoute('order_by') ?
                $this->params()->fromRoute('order_by') : 'id';
        $order = $this->params()->fromRoute('order') ?
                $this->params()->fromRoute('order') : 'ASC';

        // get location repository
        /* @var $locationRepository LocationRepository */
        $locationRepository = $this->em->getRepository('Application\Entity\Location');

        // get search phrase from query string
        $search = $this->params()->fromQuery('search');

        // if "serach" param exist get location query 
        if ($search) {
            $query = $locationRepository->getLocationQuery($search, self::DISTANCE);
            if ($query) {
                //set array adapter for object array collection
                $adapter = new ArrayAdapter($query->execute());
            } else {
                $this->flashMessenger()->addErrorMessage('Error with google service!');
                return $this->redirect()->toRoute('location');
            }
        } else {
            // get location QueryBuilder and put it to pagination adapter
            $queryBuilder = $locationRepository
                    ->createQueryBuilder('l')
                    ->orderBy('l.' . $order_by, $order);

            $adapter = new DoctrineAdapter(new ORMPaginator($queryBuilder));
        }

        //create paginator instance
        $paginator = new Paginator($adapter);
        $paginator->setDefaultItemCountPerPage(10);
        $page = (int) $this->params()->fromQuery('page');
        if ($page) {
            $paginator->setCurrentPageNumber($page);
        }
        $this->view->setVariable('paginator', $paginator);
        $this->view->setVariables(array(
            'order_by' => $order_by,
            'order' => $order
        ));
        return $this->view;
    }

    /**
     * Create new location entry
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function newAction() {
        $form = new LocationType();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $location = new Location();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                // Hydrator is used to transform array to doctrine entity object
                $hydrator = new DoctrineHydrator($this->em);
                $location = $hydrator->hydrate($form->getData(), $location);
                $this->em->persist($location);
                $this->em->flush();
                $this->sendEmail($location);
                $this->flashMessenger()->addSuccessMessage('Location "' . $location->getName() . '" successufly been saved and info email been sent!');
                return $this->redirect()->toRoute('location'); //redirect to location list
            } else {
                $this->flashMessenger()->addErrorMessage('There were errors on the form');
            }
        }

        $this->view->form = $form;
        $this->view->setTemplate('application/index/add_update.phtml'); //the same template as in edit action
        $this->view->title = 'Add new location';
        return $this->view;
    }

    /**
     * Edit existing location entry
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction() {
        $id = (int) $this->params()->fromRoute('id', null);
        if (!$id) { //if no exist id in route params - redirect to create entry
            $this->flashMessenger()->addErrorMessage('Empty id. You can create location!');
            return $this->redirect()->toRoute('location', array(
                        'action' => 'new'
            ));
        } else {
            /* @var $locationRepository LocationRepository */
            $locationRepository = $this->em->getRepository('Application\Entity\Location');
            $location = $locationRepository->find($id); //find by id
            if (!$location) { //error if no exist
                $this->flashMessenger()->addErrorMessage('Location not found. You can create it!');
                return $this->redirect()->toRoute('location', array(
                            'action' => 'new'
                ));
            }
        }

        $form = new LocationType();
        $form->setData($location->getFormData()); //populate entity data to form

        $request = $this->getRequest();
        if ($request->isPost()) { //if form is post
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $hydrator = new DoctrineHydrator($this->em);
                $location = $hydrator->hydrate($form->getData(), $location);
                $this->em->flush();
                $this->flashMessenger()->addSuccessMessage('Location "' . $location->getName() . '" successufly been updated!');
                return $this->redirect()->toRoute('location');
            } else {
                $this->flashMessenger()->addErrorMessage('There were errors on the form');
            }
        }

        $this->view->form = $form;
        $this->view->title = 'Edit "' . $location->getName() . '" location';
        $this->view->setTemplate('application/index/add_update.phtml');
        return $this->view;
    }

    /**
     * Show location details, comments, and save comments
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function showAction() {
        $id = (int) $this->params()->fromRoute('id', null);
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('Empty id!');
            return $this->redirect()->toRoute('location');
        } else {
            $locationRepository = $this->em->getRepository('Application\Entity\Location');
            /* @var $location \Application\Entity\Location */
            $location = $locationRepository->find($id);
            if (!$location) {
                $this->flashMessenger()->addErrorMessage('Location not found!');
                return $this->redirect()->toRoute('location');
            }
        }

        $form = new CommentType(); //create form
        // if comment form has been post
        $request = $this->getRequest();
        if ($request->isPost()) {
            $comment = new Comment();
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $hydrator = new DoctrineHydrator($this->em);
                $comment = $hydrator->hydrate($form->getData(), $comment);
                $comment->setLocation($location);
                $this->em->persist($comment);
                $this->em->flush();
                $this->flashMessenger()->addSuccessMessage('User "' . $comment->getEmail() . '" comment successufly been saved!');
                return $this->redirect()->toRoute('location', array('action' => 'show', 'id' => $location->getId()));
            } else {
                $this->flashMessenger()->addErrorMessage('There were errors on the form');
            }
        }

        // Create comments query used in paginator adapter
        $commentsRepository = $this->em->getRepository('Application\Entity\Comment');
        $commentsQuery = $commentsRepository
                ->createQueryBuilder('c')
                ->where('c.locationId = :locationId')
                ->setParameter('locationId', $location->getId());

        $adapter = new DoctrineAdapter(new ORMPaginator($commentsQuery));
        $paginator = new Paginator($adapter); //new paginator instance
        $paginator->setDefaultItemCountPerPage(1);
        $page = (int) $this->params()->fromQuery('page');
        if ($page) {
            $paginator->setCurrentPageNumber($page);
        }

        $this->view->form = $form;

        $this->view->paginator = $paginator;

        $this->view->location = $location;

        return $this->view;
    }

    /**
     * Delete location entry if exist and redirect to location list, or if no exist show error
     * 
     * @return \Zend\Mvc\Controller\Plugin\Redirect
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', null);
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('Empty id!');
            return $this->redirect()->toRoute('location');
        } else {
            $locationRepository = $this->em->getRepository('Application\Entity\Location');
            /* @var $location \Application\Entity\Location */
            $location = $locationRepository->find($id);
            if (!$location) {
                $this->flashMessenger()->addErrorMessage('Location not found!');
                return $this->redirect()->toRoute('location');
            }
        }

        $name = $location->getName(); // store name - used in flash message
        $this->em->remove($location);
        $this->em->flush();
        $this->flashMessenger()->addSuccessMessage('Location "' . $name . '" successufly been removed with comments!');
        return $this->redirect()->toRoute('location');
    }

    /**
     * Delete location comment entry if exist and redirect to location details, or if no exist show error
     * 
     * @return \Zend\Mvc\Controller\Plugin\Redirect
     */
    public function deleteCommentAction() {
        $id = (int) $this->params()->fromRoute('id', null);
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('Empty comment id!');
            //if no id exist redirect to referer if exist or location list
            return $this->getRequest()->getHeader('referer') ? $this->getRequest()->getHeader('referer')->getUri() : $this->redirect()->toRoute('location');
        } else {
            $commentRepository = $this->em->getRepository('Application\Entity\Comment');
            /* @var $comment \Application\Entity\Comment */
            $comment = $commentRepository->find($id);
            if (!$comment) {
                $this->flashMessenger()->addErrorMessage('Comment not found!');
                return $this->getRequest()->getHeader('referer') ? $this->getRequest()->getHeader('referer')->getUri() : $this->redirect()->toRoute('location');
            }
        }

        $name = $comment->getEmail(); //store email
        $locationId = $comment->getLocation()->getId(); //store locationId
        $this->em->remove($comment);
        $this->em->flush();
        $this->flashMessenger()->addSuccessMessage('User comment: "' . $name . '" successufly been removed with comments!');
        return $this->redirect()->toRoute('location', array('action' => 'show', 'id' => $locationId));
    }

    private function sendEmail(Location $location) {
        // get email configuration from module config
        $emailConfig = $this->getServiceLocator()->get('Config');
        $emailConfig = $emailConfig['email'];

        // create html email
        $htmlViewPart = new ViewModel();
        $htmlViewPart->setTerminal(true)
                ->setTemplate('application/index/email.phtml')
                ->setVariables(array(
                    'location' => $location
        ));
        $htmlOutput = $this->getServiceLocator()
                ->get('viewrenderer')
                ->render($htmlViewPart);

        // add html version
        $htmlPart = new MimePart($htmlOutput);
        $htmlPart->type = "text/html";

        // add text version
        $textPart = new MimePart($htmlOutput);
        $textPart->type = "text/plain";

        $body = new MimeMessage();
        $body->setParts(array($textPart, $htmlPart));

        $mail = new Mail\Message();
        $mail->setBody($body);
        $mail->setFrom($emailConfig['fromEmail'], $emailConfig['fromName']);
        $mail->addTo($emailConfig['toEmail'], $emailConfig['toName']);
        $mail->setSubject('New location!');
        $mail->getHeaders()->get('content-type')->setType('multipart/alternative');

        // Set email transport
        $transport = new SmtpTransport();
        $options = new SmtpOptions(array(
            'host' => $emailConfig['host'],
            'connection_class' => $emailConfig['connection_class'],
            'connection_config' => array(
                'ssl' => $emailConfig['ssl'],
                'username' => $emailConfig['username'],
                'password' => $emailConfig['password']
            ),
            'port' => $emailConfig['port'],
        ));
        $transport->setOptions($options);

        $transport->send($mail); //send!
    }

}
