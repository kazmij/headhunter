<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class LocationType extends Form {

    /**
     * @var InputFilter 
     */
    protected $inputFilter;

    const INTERVAL_DAYS = 7;

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('location-form');

        $this->add(array(
            'name' => 'name',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Location name',
                'label_attributes' => array(
                    'class' => 'required'
                ),
            ),
            'attributes' => array(
                'placeholder' => 'Location place name',
            ),
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'type' => 'Zend\Form\Element\Textarea',
            'options' => array(
                'label' => 'Description',
                'label_attributes' => array(
                    'class' => 'required'
                ),
            ),
            'attributes' => array(
                'placeholder' => 'Location description',
                'rows' => 5
            ),
        ));

        $this->add(array(
            'name' => 'address',
            'type' => 'Zend\Form\Element\Text',
            'options' => array(
                'label' => 'Location address',
                'label_attributes' => array(
                    'class' => 'required'
                ),
            ),
            'attributes' => array(
                'placeholder' => 'Location address',
            )
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'Zend\Form\Element\Email',
            'options' => array(
                'label' => 'Email',
                'label_attributes' => array(
                    'class' => 'required'
                ),
            ),
            'attributes' => array(
                'placeholder' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'availableFrom',
            'type' => 'Zend\Form\Element\DateSelect',
            'options' => array(
                'label' => 'Available from date',
                'label_attributes' => array(
                    'class' => 'required'
                ),
                'render_delimiters' => false,
                'day_attributes' => array(
                    'data-placeholder' => 'Day',
                    'class' => 'form-control',
                    'required' => true,
                    'style' => 'display: inline-block; width: 20%; float: left;',
                ),
                'month_attributes' => array(
                    'data-placeholder' => 'Month',
                    'class' => 'form-control',
                    'required' => true,
                    'style' => 'display: inline-block; width: 20%; float: left;',
                ),
                'year_attributes' => array(
                    'data-placeholder' => 'Year',
                    'class' => 'form-control',
                    'required' => true,
                    'style' => 'display: inline-block; width: 20%; float: left;',
                )
            ),
            'attributes' => array(
                'placeholder' => 'Available from date',
            )
        ));

        $this->add(array(
            'name' => 'availableTo',
            'type' => 'Zend\Form\Element\DateSelect',
            'options' => array(
                'label' => 'Available to date',
                'label_attributes' => array(
                    'class' => 'required'
                ),
                'render_delimiters' => false,
                'day_attributes' => array(
                    'data-placeholder' => 'Day',
                    'class' => 'form-control',
                    'required' => true,
                    'style' => 'display: inline-block; width: 20%; float: left;',
                ),
                'month_attributes' => array(
                    'data-placeholder' => 'Month',
                    'class' => 'form-control',
                    'required' => true,
                    'style' => 'display: inline-block; width: 20%; float: left;',
                ),
                'year_attributes' => array(
                    'data-placeholder' => 'Year',
                    'class' => 'form-control',
                    'required' => true,
                    'style' => 'display: inline-block; width: 20%; float: left;',
                )
            ),
            'attributes' => array(
                'placeholder' => 'Available to date',
            )
        ));

        $this->add(array(
            'name' => 'latitude',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'class' => 'latitudeField',
            ),
        ));

        $this->add(array(
            'name' => 'longitude',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'class' => 'longitudeField',
            ),
        ));

        $this->add(array(
            'type' => 'Submit',
            'name' => 'submit',
            'options' => array(
                'label' => 'Submit',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-success'
            )
        ));

        $this->setInputFilter($this->getInputFilter());
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name' => 'name',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 50,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'description',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 300,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'address',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 150,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'email',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'latitude',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter valid address to find latitude!'
                            ),
                        ),
                    )
                )
            ));

            $inputFilter->add(array(
                'name' => 'longitude',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter valid address to find longitude!'
                            ),
                        ),
                    )
                )
            ));

            $validatorChainFrom = new \Zend\Validator\ValidatorChain();
            $validatorChainFrom->addValidator($this->getDateValidator('availableFrom', true));
            $validatorChainFrom->addValidator($this->getDateValidator('availableFrom'));
            $availableFrom = new \Zend\InputFilter\Input('availableFrom');
            $availableFrom
                    ->setRequired(true)
                    ->setValidatorChain($validatorChainFrom);
            $inputFilter->add($availableFrom);

            $validatorChainTo = new \Zend\Validator\ValidatorChain();
            $validatorChainTo->addValidator($this->getDateValidator('availableTo', true));
            $validatorChainTo->addValidator($this->getDateValidator('availableTo'));
            $availableTo = new \Zend\InputFilter\Input('availableTo');
            $availableTo
                    ->setRequired(true)
                    ->setValidatorChain($validatorChainTo);
            $inputFilter->add($availableTo);

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    private function getDateValidator($field, $intervalValidator = false) {
        if ($intervalValidator) {
            $allowedStartDate = new \DateTime('now');
            $interval = new \DateInterval('P' . self::INTERVAL_DAYS . 'D');
            $allowedStartDate->add($interval);

            $callback = new \Zend\Validator\Callback(function ($value, $context = array()) use ($allowedStartDate) {
                $date = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day']);
                return $date->getTimestamp() > $allowedStartDate->getTimestamp();
            }
            );
            $callback->setMessage('Date "' . $this->get($field)->getLabel() . '" must be greater than ' . $allowedStartDate->format('Y-m-d') . ' date (now +7 days)');
            return $callback;
        }

        $type = '';

        if ($field === 'availableTo') {
            $callback = new \Zend\Validator\Callback(function ($value, $context = array()) {
                $dateTo = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day']);
                if (!empty($context['availableFrom'])) {
                    $dateFrom = new \DateTime($context['availableFrom']['year'] . '-' . $context['availableFrom']['month'] . '-' . $context['availableFrom']['day']);
                    return $dateTo->getTimestamp() > $dateFrom->getTimestamp();
                } else {
                    return true;
                }
            }
            );
            $type = 'greater';
        } else if ($field === 'availableFrom') {
            $callback = new \Zend\Validator\Callback(function ($value, $context = array()) {
                $dateFrom = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day']);
                if (!empty($context['availableTo'])) {
                    $dateTo = new \DateTime($context['availableTo']['year'] . '-' . $context['availableTo']['month'] . '-' . $context['availableTo']['day']);
                    return $dateFrom->getTimestamp() < $dateTo->getTimestamp();
                } else {
                    return true;
                }
            }
            );
            $type = 'less';
        }
        $callback->setMessage('Date "' . $this->get($field)->getLabel() . '" must be ' . $type . ' than "Available to" date!');

        return $callback;
    }

}
