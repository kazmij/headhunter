<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * CommentType 
 * 
 * Create comment entry form
 */
class CommentType extends Form {

    /**
     * @var InputFilter 
     */
    protected $inputFilter;

    public function __construct($name = null) {
        // set name
        parent::__construct('comment-form');

        $this->add(array(
            'name' => 'email',
            'type' => 'Zend\Form\Element\Email',
            'options' => array(
                'label' => 'Email',
                'label_attributes' => array(
                    'class' => 'required'
                ),
            ),
            'attributes' => array(
                'placeholder' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'comment',
            'type' => 'Zend\Form\Element\Textarea',
            'options' => array(
                'label' => 'Description',
                'label_attributes' => array(
                    'class' => 'required'
                ),
            ),
            'attributes' => array(
                'placeholder' => 'Comment',
                'rows' => 5
            ),
        ));

        $this->add(array(
            'type' => 'Submit',
            'name' => 'submit',
            'options' => array(
                'label' => 'Save',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-success'
            )
        ));

        // Set data input filter (validation and filter)
        $this->setInputFilter($this->getInputFilter());
    }

    /**
     * Get input form filter
     * 
     * @return InputFilter
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name' => 'comment',
                'required' => true, //it's required field
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array( //set length validator
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 300,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'email',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                    ),
                ),
            ));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
