<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\Base;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Location Entity
 *
 * @ORM\Entity(repositoryClass="Application\Repository\LocationRepository")
 * @ORM\Table(name="location")
 */
class Location extends Base {

    /**
     * @var integer

     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="text", length=50, nullable=false)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", length=300, nullable=false)
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(name="address", type="text", length=250, nullable=false)
     */
    protected $address;

    /**
     * @var string
     * @ORM\Column(name="email", type="text", length=60, nullable=false)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(name="available_from_date", type="datetime", nullable=false)
     */
    protected $availableFrom;

    /**
     * @var string
     * @ORM\Column(name="available_to_date", type="datetime", nullable=false)
     */
    protected $availableTo;

    /**
     * @var string
     * @ORM\Column(name="latitude", type="text", length=30, nullable=true)
     */
    protected $latitude;

    /**
     * @var string
     * @ORM\Column(name="longitude", type="text", length=30, nullable=true)
     */
    protected $longitude;

    /**
     * Comment[]
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="location", cascade={"remove", "persist"})
     */
    protected $comments;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Location
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Location
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Location
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Location
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set availableFrom
     *
     * Transform array data to \DateTime object
     * 
     * @param \DateTime $availableFrom
     *
     * @return Location
     */
    public function setAvailableFrom($availableFrom) {
        if (!$availableFrom instanceof \DateTime) {
            $availableFrom = new \DateTime($availableFrom['year'] . '-' . $availableFrom['month'] . '-' . $availableFrom['day']);
        }
        $this->availableFrom = $availableFrom;

        return $this;
    }

    /**
     * Get availableFrom
     *
     * @return \DateTime
     */
    public function getAvailableFrom() {
        return $this->availableFrom;
    }

    /**
     * Set availableTo
     *
     * Transform array data to \DateTime object
     * 
     * @param \DateTime $availableTo
     *
     * @return Location
     */
    public function setAvailableTo($availableTo) {
        if (!$availableTo instanceof \DateTime) {
            $availableTo = new \DateTime($availableTo['year'] . '-' . $availableTo['month'] . '-' . $availableTo['day']);
        }
        $this->availableTo = $availableTo;

        return $this;
    }

    /**
     * Get availableTo
     *
     * @return \DateTime
     */
    public function getAvailableTo() {
        return $this->availableTo;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Location
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Location
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * Convert entity to array - used in forms
     * @return array
     */
    public function getFormData() {
        return array(
            'name' => $this->name,
            "description" => $this->description,
            "address" => $this->address,
            "email" => $this->email,
            "latitude" => $this->latitude,
            "longitude" => $this->longitude,
            "availableFrom" => array('year' => $this->availableFrom->format('Y'), 'month' => $this->availableFrom->format('m'), 'day' => $this->availableFrom->format('d')),
            "availableTo" => array('year' => $this->availableTo->format('Y'), 'month' => $this->availableTo->format('m'), 'day' => $this->availableTo->format('d'))
        );
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection(); //comments collection
    }

    /**
     * Add comment
     *
     * @param \Application\Entity\Comment $comment
     *
     * @return Location
     */
    public function addComment(\Application\Entity\Comment $comment) {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \Application\Entity\Comment $comment
     */
    public function removeComment(\Application\Entity\Comment $comment) {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments() {
        return $this->comments;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Location
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Location
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
