<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\Base;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Comment
 * 
 * @ORM\Entity(repositoryClass="Application\Repository\CommentRepository")
 * @ORM\Table(name="comment")
 */
class Comment extends Base {

    /**
     * @var integer

     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", length=300, nullable=false)
     */
    protected $comment;

    /**
     * @var string
     * @ORM\Column(name="email", type="text", length=60, nullable=false)
     */
    protected $email;

    /**
     * Location
     * 
     * Set relation with location entity
     * 
     * @ORM\ManyToOne(targetEntity="Location", inversedBy="comments", cascade={"persist"})
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $location;

    /**
     * @var integer

     * @ORM\Column(name="location_id", type="integer")
     */
    protected $locationId;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Comment
     */
    public function setComment($comment) {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Comment
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set location
     *
     * @param \Application\Entity\Location $location
     *
     * @return Comment
     */
    public function setLocation(\Application\Entity\Location $location = null) {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \Application\Entity\Location
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set locationId
     *
     * @param integer $locationId
     *
     * @return Comment
     */
    public function setLocationId($locationId) {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId
     *
     * @return integer
     */
    public function getLocationId() {
        return $this->locationId;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Comment
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}
