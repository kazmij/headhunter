<?php

namespace Application\Entity;

/**
 * Location
 */
class Base {

    public function setData($data) {
        if (method_exists($this, 'dataTransform')) {
            $data = $this->dataTransform($data);
        }
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                if (is_array($this->{$key})) {
                    $this->{$key}[] = $value;
                } else {
                    $this->{$key} = $value;
                }
            }
        }

        return $this;
    }

}
