window.onload = function () {
    $('.confirmAction').each(function(i){
        $(this).attr('data-href', $(this).attr('href')).attr('href', '#');
    });
    
    $('body').on('click', '.confirmAction', function (e) {
        e.preventDefault();
        var modalModel = $('.modalModelConfirm')[0].outerHTML,
                handler = $(this);
        if (modalModel) {
            var myModal = $(modalModel),
                    date = new Date(),
                    myModalClass = 'modal-' + date.getTime();
            myModal
                    .removeClass('modalModelConfirm')
                    .addClass(myModalClass);
            if (handler.attr('data-confirm-title')) {
                myModal.find('.modal-title').html(handler.attr('data-confirm-title'));
            }
            if (handler.attr('data-confirm-content')) {
                myModal.find('.modal-body p').html(handler.attr('data-confirm-content'));
            }
            $('body').append(myModal);
            myModal.find('.btnConfirm').on('click', function (e) {
                e.preventDefault();
                myModal.modal('hide');
                myModal.remove();
                window.location.href = handler.attr('data-href');
                return false;
            })
            myModal.modal();
        } else {
            alert('No modal found...')
        }
        return false;
    });
};
